//simple uart top, loop rx back to tx
 
module uart (clk, rst, rxd, txd, baud_freq, baud_limit, rx_data, rx_int, tx_data, tx_start, tx_int, tx_busy, loopback);
input       clk;
input       rst;
input       rxd;
output      txd;
input [11:0] baud_freq;
input [15:0] baud_limit;
output [7:0] rx_data;
output 	     rx_int;
input [7:0]  tx_data;
input 	     tx_start;
output       tx_int;
output 	     tx_busy;
input 	     loopback;

wire        clk_ovrsamp; 
wire [7:0]  rx_data_ir;
wire 	    rx_int_ir;
wire [7:0]  tx_data_ir;
wire        tx_start_ir;
wire 	    tx_int_ir;
wire        tx_busy_ir;
 
reg [7:0]   tx_data_loop;
reg         tx_start_loop;
 
 
baud_gen u_baud_gen(.clk(clk), 
        .rst(rst), 
        .clk_ovrsamp(clk_ovrsamp),
        .baud_freq(baud_freq),
        .baud_limit(baud_limit));
 
uart_rx u_uart_rx(.clk(clk), 
        .rst(rst),
        .clk_ovrsamp(clk_ovrsamp),
        .rxd(rxd),
        .rx_data(rx_data_ir),
        .rx_int(rx_int_ir));
 
uart_tx u_uart_tx(.clk(clk),
        .rst(rst),
        .clk_ovrsamp(clk_ovrsamp),
        .txd(txd), 
        .tx_data(tx_data_ir),
        .new_tx(tx_start_ir),
        .tx_int(tx_int_ir),
        .tx_busy(tx_busy_ir));
 
assign rx_data = rx_data_ir;
assign rx_int  = rx_int_ir;
assign tx_int  = tx_int_ir;
assign tx_busy = tx_busy_ir;
 
assign tx_data_ir  = loopback ? tx_data_loop  : tx_data;
assign tx_start_ir = loopback ? tx_start_loop : tx_start;
 
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        tx_data_loop <= 8'h0;
    else if (rx_int_ir & ~tx_busy_ir)
        tx_data_loop <= rx_data_ir;
end

always @(posedge clk or negedge rst)
begin
    if (!rst)
        tx_start_loop <= 1'b0;
    else if (rx_int_ir & ~tx_busy_ir)
        tx_start_loop <= 1'b1;
    else
        tx_start_loop <= 1'b0;
end
 
endmodule