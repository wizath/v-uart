module uart_rx(clk,
        rst,
        clk_ovrsamp,
        rxd,
        rx_data,
        rx_int);
input clk;
input rst;
input clk_ovrsamp;
input rxd;
output [7:0] rx_data;
output rx_int;
 
reg [7:0]   rx_data;
reg         rx_int;
 
reg [7:0]   rx_data_pre;
reg [1:0]   rxdn;
reg [3:0]   count;
reg [3:0]   bit_count;
reg         rx_busy;
 
wire        bit_mid;
wire        bit_end;
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        rxdn <= 2'b11;
    else
        rxdn <= {rxdn[0], rxd};
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        count <= 4'h0;
    else if ((rx_busy | (rxdn[1] == 1'b0)) & clk_ovrsamp)
        count <= count + 1'b1;
    else if (!rx_busy & clk_ovrsamp)
        count <= 4'h0;
end
 
assign bit_mid = ((count == 4'b0111) & clk_ovrsamp);
assign bit_end = ((count == 4'b1111) & clk_ovrsamp);
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        rx_busy <= 1'b0;
    else if (!rx_busy & bit_mid)
        rx_busy <= 1'b1;
    else if (rx_busy & bit_end & (bit_count == 4'd9))
        rx_busy <= 1'b0;
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        bit_count <= 4'h0;
    else if (rx_busy & bit_mid)
        bit_count <= bit_count + 1'b1;
    else if (!rx_busy)
        bit_count <= 4'h0;
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        rx_data_pre <= 8'h0;
    else if (bit_mid)
        rx_data_pre <= {rxdn[1], rx_data_pre[7:1]};
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst) begin
        rx_data <= 8'h0;
        rx_int <= 1'b0;
    end else if (bit_end & (bit_count == 4'd8)) begin
        rx_data <= rx_data_pre;
        rx_int <= 1'b1;
    end else
        rx_int <= 1'b0;
end
 
endmodule