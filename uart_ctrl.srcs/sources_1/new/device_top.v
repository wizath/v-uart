`timescale 1ns / 1ps

module device_top (
        input clk,
        input rst,
        input uart_rx,
        output uart_tx,
        output led,
        output led2
    );
    
wire clk_ovrsamp;
wire [7:0]  rx_data_ir;
wire 	    rx_int_ir;
wire [7:0]  tx_data_ir;
wire        tx_start_ir;
wire 	    tx_int_ir;
wire        tx_busy_ir;
wire        rrst;

reg [7:0]   tx_data_loop;
reg         tx_start_loop;

baud_gen u_baud_gen (
            .clk(clk),
            .rst(rst),
            .clk_ovrsamp(clk_ovrsamp),
            .baud_freq(12'h60),
            .baud_limit(16'h211)
            );
            
uart_rx u_uart_rx(
            .clk(clk), 
            .rst(rst),
            .clk_ovrsamp(clk_ovrsamp),
            .rxd(uart_rx),
            .rx_data(rx_data_ir),
            .rx_int(rx_int_ir)
            );
             
uart_tx u_uart_tx(
        .clk(clk),
        .rst(rst),
        .clk_ovrsamp(clk_ovrsamp),
        .txd(uart_tx), 
        .tx_data(tx_data_ir),
        .new_tx(tx_start_ir),
        .tx_int(tx_int_ir),
        .tx_busy(tx_busy_ir)
        );
        
//assign rrst = !rst;
assign led = rst;
assign led2 = tx_busy;
assign rx_data = rx_data_ir;
assign rx_int  = rx_int_ir;
assign tx_int  = tx_int_ir;
assign tx_busy = tx_busy_ir;
 
assign tx_data_ir  = tx_data_loop;
assign tx_start_ir = tx_start_loop;

always @(posedge clk or negedge rst)
begin
    if (!rst)
        tx_data_loop <= 8'h0;
    else if (rx_int_ir & ~tx_busy_ir)
        tx_data_loop <= rx_data_ir;
end

always @(posedge clk or negedge rst)
begin
    if (!rst)
        tx_start_loop <= 1'b0;
    else if (rx_int_ir & ~tx_busy_ir)
        tx_start_loop <= 1'b1;
    else
        tx_start_loop <= 1'b0;
end

endmodule
