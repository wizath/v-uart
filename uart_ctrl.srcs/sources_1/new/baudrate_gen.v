//generate 16x oversampling clock (clk_oversamp)
//receive baud_freq = 16*baud_rate / gcd(global_clock_freq, 16*baud_rate)
//and baud_limit = (global_clock_freq /gcd(global_clock_freq, 16*baud_rate)) - baud_freq

module baud_gen(clk, rst, clk_ovrsamp, baud_freq, baud_limit);
input           clk;
input           rst;
output          clk_ovrsamp;
input [11:0]    baud_freq;
input [15:0]    baud_limit;
 
reg clk_ovrsamp;
reg [15:0] count;
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        count <= 16'h0;
    else if (count >= baud_limit)
        count <= count - baud_limit;
    else
        count <= count + baud_freq;
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        clk_ovrsamp <= 1'b0;
    else if (count >= baud_limit)
        clk_ovrsamp <= 1'b1;
    else
        clk_ovrsamp <= 1'b0;
end
 
endmodule