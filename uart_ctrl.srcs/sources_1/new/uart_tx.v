module uart_tx(clk, 
        rst, 
        clk_ovrsamp, 
        txd, 
        tx_data, 
        new_tx, 
        tx_int, 
        tx_busy);
input       clk;
input       rst;
input       clk_ovrsamp;
output      txd;
input [7:0] tx_data;
input       new_tx;
output      tx_int;
output      tx_busy;
 
reg     txd;
reg     tx_int;
reg     tx_busy;
reg [3:0]   count;
reg [3:0]   bit_count;
reg [8:0]   tx_data_reg;
 
wire        bit_end;
assign bit_end = (count == 4'b1111) & clk_ovrsamp;
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        count <= 4'h0;
    else if (tx_busy & clk_ovrsamp)
        count <= count + 1'b1;
    else if (!tx_busy & clk_ovrsamp)
        count <= 1'b0;
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        tx_busy <= 1'b0;
    else if (!tx_busy & new_tx)
        tx_busy <= 1'b1;
    else if (tx_busy & bit_end & (bit_count == 4'd9))
        tx_busy <= 1'b0;
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        bit_count <= 4'h0;
    else if (tx_busy & bit_end)
        bit_count <= bit_count + 1'b1;
    else if (!tx_busy)
        bit_count <= 4'h0;
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        tx_data_reg <= 9'h0;
    else if (!tx_busy)
        tx_data_reg <= {tx_data, 1'b0};
    else if (tx_busy & bit_end)
        tx_data_reg <= {1'b1, tx_data_reg[8:1]};
end
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        txd <= 1'b1;
    else if (tx_busy)
        txd <= tx_data_reg[0];
    else
        txd <= 1'b1;
end
 
 
always @(posedge clk or negedge rst)
begin
    if (!rst)
        tx_int <= 1'b0;        
    else if (tx_busy & bit_end & (bit_count == 4'd9))
        tx_int <= 1'b1;
    else
        tx_int <= 1'b0;
end
 
endmodule