`timescale 1ns / 100ps
 
module uart_tb ();
 
  reg clk, rst;
  wire txd, rxd;
  wire [7:0] rx_data_local;
  wire rx_int_local;
  reg [7:0]	tx_data_local;
  reg tx_start_local;
  wire tx_int_local;
  wire tx_busy_local;
 
uart dut_local (
	.clk (clk),
	.rst (rst),
	.rxd (rxd),
	.txd (txd),
	.baud_freq 	(12'h60),
	.baud_limit (16'h211),
	.rx_data (rx_data_local),
	.rx_int (rx_int_local),
	.tx_data (tx_data_local),
	.tx_start (tx_start_local),
	.tx_int (tx_int_local),
	.tx_busy (tx_busy_local),
	.loopback (1'b0)	
	);
 
uart dut_remote (
	.clk (clk),
	.rst (rst),
	.rxd (txd),
	.txd (rxd),
	.baud_freq 	(12'h60),
	.baud_limit (16'h211),
	.rx_data (),
	.rx_int (),
	.tx_data (8'b0),
	.tx_start (1'b0),
	.tx_int (),
	.tx_busy (),
	.loopback (1'b1)
);
 
 
//create a 20Mhz clock
always 
  #40 clk = ~clk;
 
initial
begin
   clk = 1'b0;
   rst = 1'b0;
   tx_data_local = 8'b0;
   tx_start_local =1'b0;
   #70;
   rst = 1'b1;
   tx_data_local = 8'h5a;
   #5000;
   tx_start_local = 1'b1;
   #1000;
   tx_start_local = 1'b0;
 
   
end
 
endmodule